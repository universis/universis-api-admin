import { ApplicationService, TraceUtils } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import { cloneDeep, groupBy } from 'lodash';

export class PermissionExtensionService extends ApplicationService {
  constructor(app) {
    // call super constructor
    super(app);
    // install service
    this.install();
  }

  install() {
    const application = this.getApplication();
    // create a context through the parent application
    const context = application.createContext();
    if (context == null) {
      return;
    }
    // wrap async operations in a function
    (async () => {
      // get all special permissions
      let specialPermissions = await context
        .model('SpecialPermission')
        // ensure that filter is defined
        // since this service handles self privileges
        .where('filter').notEqual(null)
        .expand('account')
        .silent()
        .getAllItems();
      // if none are defined, exit
      if (
        !(Array.isArray(specialPermissions) && specialPermissions.length > 0)
      ) {
        return;
      }
      // group permissions by appliesTo (dataModel)
      // to prevent multiple setModelDefinition operations
      specialPermissions = groupBy(specialPermissions, (item) => {
        return [item.appliesTo];
      });
      // get schema loader
      const schemaLoader = application
        .getConfiguration()
        .getStrategy(SchemaLoaderStrategy);
      if (schemaLoader == null) {
        return;
      }
      for (const [appliesTo, privileges] of Object.entries(
        specialPermissions
      )) {
        // get model definition
        const modelDefinition = cloneDeep(
          schemaLoader.getModelDefinition(appliesTo)
        );
        if (modelDefinition == null) {
          continue;
        }
        let updateModelDefinition = false;
        // ensure privileges
        modelDefinition.privileges = modelDefinition.privileges || [];
        // enumerate privileges related to that data model
        privileges.forEach((privilege) => {
          const targetPrivilege = {
            mask: privilege.mask,
            account: privilege.account && privilege.account.alternateName,
            filter: privilege.filter,
            type: 'self',
          };
          // try to find if privilege already exists
          const privilegeExists = modelDefinition.privileges.find((item) => {
            return (
              item.mask >= targetPrivilege.mask &&
              item.account === targetPrivilege.account &&
              item.filter === targetPrivilege.filter &&
              item.type === targetPrivilege.type
            );
          });
          // if it does, exit
          if (privilegeExists) {
            return;
          }
          // push privilege to privileges collection
          modelDefinition.privileges.push(targetPrivilege);
          updateModelDefinition = true;
        });
        if (!updateModelDefinition) {
          continue;
        }
        // and update model definition
        schemaLoader.setModelDefinition(modelDefinition);
      }
    })()
      .then(() => {
        TraceUtils.info(
          'Services: PermissionExtensionService service has been successfully installed.'
        );
      })
      .catch((err) => {
        // just log the error
        TraceUtils.error(
          'An error occured while trying to install PermissionExtensionService'
        );
        TraceUtils.error(err);
      });
  }
}
