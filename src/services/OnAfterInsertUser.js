import { RandomUtils } from '@themost/common';
import { DataObjectState } from '@themost/data';
import crypto from 'crypto';

function toHash(value, algorithm) {
    if (algorithm === 'clear' || algorithm === 'none') {
        return value;
    }
    const hash = crypto.createHash(algorithm);
    hash.update(value);
    return hash.digest('hex');
}

/**
 * @param {import('@themost/data').DataEventArgs} event 
 */
async function afterSaveAsync(event) {
    if (event.state !== DataObjectState.Insert) {
        return;
    }
    const context = event.model.context;
    const UserActivations = context.model('UserActivation').silent();
    const userActivation = await UserActivations.where('id').equal(event.target.id).getItem();
    // get configuration options
    const configurationOptions = context.getConfiguration().getSourceAt('settings/universis/userActivation');
    // assign default options
    const options = Object.assign({
        length: 20,
        algorithm: 'sha256'
    }, configurationOptions);
    // generate activation code
    const activationCode = RandomUtils.randomChars(options.length).toUpperCase();

    // create hashed activation code
    const hashedActivationCode = '{' + options.algorithm + '}' + toHash(activationCode, options.algorithm);

    if (userActivation) {
        // update user activation
        Object.assign(userActivation, {
            activationAttributes: {
                activationCode
            },
            activationCode: hashedActivationCode,
            userActivation: true
        })
        // save changes
        await UserActivations.save(userActivation);
    } else {
        // create new user activation
        const newUserActivation = {
            id: event.target.id,
            name: event.target.name,
            activationAttributes: {
                activationCode
            },
            activationCode: hashedActivationCode,
            userActivation: true
        };
        // save changes
        await UserActivations.insert(newUserActivation);
    }
}

function afterSave(event, callback) {
    afterSaveAsync(event).then(() => {
        callback();
    }).catch((err) => {
        callback(err);
    });
}

export {
    afterSave
}