import { ApplicationService } from '@themost/common';
import { DataConfigurationStrategy } from '@themost/data';
import path from 'path';


class CreateUserActivationAfterUserReference extends ApplicationService {
    constructor(app) {
        super(app);
        this.install();
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get model definition
        const UserModel = configuration.getModelDefinition('UserReference');
        UserModel.eventListeners = UserModel.eventListeners || [];
        // try to find event listener
        const type = path.resolve(__dirname, './OnAfterInsertUser');
        let found = UserModel.eventListeners.find(listener => { return listener.type === type; });
        if (found == null) {
            // add event listener
            UserModel.eventListeners.push({
                type
            });
            // and reset model definition
            configuration.setModelDefinition(UserModel);
        }
    }

    uninstall() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get model definition
        const UserModel = configuration.getModelDefinition('UserReference');
        UserModel.eventListeners = UserModel.eventListeners || [];
        // try to find event listener
        const type = path.resolve(__dirname, './OnAfterInsertUser');
        let found = UserModel.eventListeners.findIndex(listener => { return listener.type === type; });
        if (found >= 0) {
            UserModel.eventListeners.splice(found, 1);
            // and reset model definition
            configuration.setModelDefinition(UserModel);
        }
    }

}

export {
    CreateUserActivationAfterUserReference
}