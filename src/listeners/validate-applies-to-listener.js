import { DataError } from '@themost/common';

export function beforeSave(event, callback) {
  try {
    if (!event.target.hasOwnProperty('appliesTo')) {
      return callback();
    }
    const context = event.model.context;
    // validate entity type
    const entityType = context.model(event.target.appliesTo);
    if (entityType == null) {
      return callback(
        Object.assign(
          new DataError(
            'E_APPLIES_TO',
            'The specified entity type to which this item applies to cannot be found in the current application context.',
            null,
            event.model.name,
            'appliesTo'
          ),
          {
            statusCode: 409,
          }
        )
      );
    }
    return callback();
  } catch (err) {
    return callback(err);
  }
}
