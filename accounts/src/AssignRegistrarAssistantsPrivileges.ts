// @ts-ignore
import { ApplicationService, IApplication } from '@themost/common';
import { AccountPrivilegeConfiguration } from './PrivilegeAssignmentService';

export class AssignRegistrarAssistantsPrivileges extends ApplicationService {
  constructor(app: IApplication) {
    super(app);
    // get upgradeAccounts configuration
    const accountsConfiguration: AccountPrivilegeConfiguration[] =
      app
        .getConfiguration()
        .getSourceAt('settings/universis/admin/upgradeAccounts/accounts') || [];
    // prepare the RegistrarAssistants group configuration
    const registrarAssistantsConfiguration: AccountPrivilegeConfiguration = {
      from: {
        name: 'Registrar',
        method: 'departments()',
      },
      to: {
        name: 'RegistrarAssistants',
        method: 'studyPrograms()',
        methodEntityType: 'StudyProgram',
      },
    };
    // ensure that the configuration does not exist
    const configurationExists = accountsConfiguration.find((account) => {
      return (
        account.from.name === registrarAssistantsConfiguration.from.name &&
        account.from.method === registrarAssistantsConfiguration.from.method &&
        account.to.name === registrarAssistantsConfiguration.to.name &&
        account.to.method === registrarAssistantsConfiguration.to.method &&
        account.to.methodEntityType ===
          registrarAssistantsConfiguration.to.methodEntityType
      );
    });
    if (configurationExists) {
      return;
    }
    // push and set the configuration
    accountsConfiguration.push(registrarAssistantsConfiguration);
    app
      .getConfiguration()
      .setSourceAt(
        'settings/universis/admin/upgradeAccounts/accounts',
        accountsConfiguration
      );
  }
}
