// @ts-ignore
import { ApplicationService, IApplication } from '@themost/common';
import { AccountPrivilegeConfiguration } from './PrivilegeAssignmentService';
// @ts-ignore
import { PermissionMask } from '@themost/data';

export class AssignRegistrarViewersPrivileges extends ApplicationService {
  constructor(app: IApplication) {
    super(app);
    // get upgradeAccounts configuration
    const accountsConfiguration: AccountPrivilegeConfiguration[] =
      app
        .getConfiguration()
        .getSourceAt('settings/universis/admin/upgradeAccounts/accounts') || [];
    // prepare the RegistrarViewers group configuration
    const registrarViewersConfiguration: AccountPrivilegeConfiguration = {
      from: {
        name: 'Registrar',
        method: 'departments()',
      },
      to: {
        name: 'RegistrarViewers',
        method: 'departments()',
        methodEntityType: 'Department',
        mask: PermissionMask.Read,
      },
    };
    // ensure that the configuration does not exist
    const configurationExists = accountsConfiguration.find((account) => {
      return (
        account.from.name === registrarViewersConfiguration.from.name &&
        account.from.method === registrarViewersConfiguration.from.method &&
        account.to.name === registrarViewersConfiguration.to.name &&
        account.to.method === registrarViewersConfiguration.to.method &&
        account.to.mask === registrarViewersConfiguration.to.mask &&
        account.to.methodEntityType ===
          registrarViewersConfiguration.to.methodEntityType
      );
    });
    if (configurationExists) {
      return;
    }
    // push and set the configuration
    accountsConfiguration.push(registrarViewersConfiguration);
    app
      .getConfiguration()
      .setSourceAt(
        'settings/universis/admin/upgradeAccounts/accounts',
        accountsConfiguration
      );
  }
}
